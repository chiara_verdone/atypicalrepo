//
//  HomeView.swift
//  AtypicalProject
//
//  Created by verdonechristian on 03/02/2020.
//  Copyright © 2020 verdonechiara. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        NavigationView{
            VStack{
                NavigationLink(destination: Map()){
                    Text("Use a map")
                        .bold()
                        .font(.largeTitle)
                    }
                    .isDetailLink(true)
                Spacer()
            }
            .navigationBarTitle("Home", displayMode: .inline)
            .colorScheme(.dark)
        }.navigationBarBackButtonHidden(true)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
