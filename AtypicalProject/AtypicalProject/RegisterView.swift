//
//  RegisterView.swift
//  AtypicalProject
//
//  Created by verdonechristian on 03/02/2020.
//  Copyright © 2020 verdonechiara. All rights reserved.
//

import SwiftUI

struct RegisterView: View {
    @State var mail: String
    @State var pass: String
    @State var check: String
    var body: some View {
        VStack{
            Image("gattino")
            .resizable()
                .padding()
                .aspectRatio(contentMode: .fit)
            Text("Inserisci e-mail:")
                .font(.body)
            TextField("  E-mail", text: $mail)
                .border(Color.gray, width: 2)
                .keyboardType(UIKeyboardType.emailAddress)
            Text("Inserisci Password:")
                .font(.body)
            TextField("  Password", text: $pass)
                .border(Color.gray, width: 2)
                .keyboardType(UIKeyboardType.default)
            Text("Inserisci di nuovo la Password:")
                .font(.body)
            TextField("  Password", text: $pass)
                .border(Color.gray, width: 2)
                .keyboardType(UIKeyboardType.default)
            NavigationLink(destination: LoginView()){
                Text("Register")
                .bold()
                    .font(.largeTitle)
            }
            Spacer()
        }
        .padding([.top,.horizontal])
    
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView(mail: "", pass: "", check: "")
    }
}
