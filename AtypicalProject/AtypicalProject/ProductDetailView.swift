//
//  ProductDetailView.swift
//  AtypicalProject
//
//  Created by verdonechiara on 03/02/2020.
//  Copyright © 2020 verdonechiara. All rights reserved.
//

import SwiftUI

struct ProductDetailView: View {
    @State var imageName = "gattino"
    var body: some View {
        VStack{
            HStack{
                Image(imageName)
                    .resizable()
                    .frame(width: 100, height: 100)
                    .cornerRadius(15)
                    .padding()
                
                    Spacer()
            }
            Spacer()
        }
        
    }
}

struct ProductDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ProductDetailView()
    }
}
