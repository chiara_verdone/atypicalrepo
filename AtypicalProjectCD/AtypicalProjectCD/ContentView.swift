//
//  ContentView.swift
//  AtypicalProjectCD
//
//  Created by verdonechiara on 04/02/2020.
//  Copyright © 2020 verdonechiara. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
