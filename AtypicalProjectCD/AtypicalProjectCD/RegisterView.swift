//
//  RegisterView.swift
//  AtypicalProject
//
//  Created by verdonechristian on 03/02/2020.
//  Copyright © 2020 verdonechiara. All rights reserved.
//

import SwiftUI

struct RegisterView: View {
    @State var mail: String
    @State var pass: String
    @State var check: String
    var body: some View {
        NavigationView{
            ScrollView{
                VStack{
                    Image("gattino")
                        .resizable()
                        .padding()
                        .aspectRatio(contentMode: .fit)
                    HStack{
                        Text("Inserisci e-mail:")
                            .font(.body)
                            .foregroundColor(Color.gray)
                        TextField("  E-mail", text: $mail)
                            .border(Color.gray, width: 2)
                            .keyboardType(UIKeyboardType.emailAddress)
                        .padding()
                    }
                    HStack{
                        Text("Inserisci Password:")
                            .font(.body)
                            .foregroundColor(Color.gray)
                        TextField("  Password", text: $pass)
                            .border(Color.gray, width: 2)
                            .keyboardType(UIKeyboardType.default)
                        .padding()
                    }
                    HStack{
                        Text("Inserisci di nuovo la Password:")
                            .font(.body)
                            .foregroundColor(Color.gray)
                        TextField("  Password", text: $pass)
                            .border(Color.gray, width: 2)
                            .keyboardType(UIKeyboardType.default)
                        .padding()
                    }
                    NavigationLink(destination: LoginView()){
                        Text("Register")
                            .bold()
                            .font(.title)
                            
                    .padding()
                }
                Spacer()
            }
            .padding([.top,.horizontal])
            .navigationBarTitle("Registration", displayMode: .inline)
            }
        }
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView(mail: "", pass: "", check: "")
    }
}
