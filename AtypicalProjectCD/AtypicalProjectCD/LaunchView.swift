//
//  ContentView.swift
//  AtypicalProject
//
//  Created by verdonechiara on 29/01/2020.
//  Copyright © 2020 verdonechiara. All rights reserved.
//

import SwiftUI

struct LaunchView: View {
    var body: some View {
        NavigationView{
            VStack{
                Image("gattino")
                    .resizable()
                    .frame(height: 300)
                    .padding()
                
                NavigationLink(destination: HomeView()) {
                    Text("Vada non cincischi!")
                        .fontWeight(.bold)
                    .padding()
                        .border(Color.blue)
                        .buttonStyle(PlainButtonStyle())
                }
            }
        }
        
    }
}
    
    struct LaunchView_Previews: PreviewProvider {
        static var previews: some View {
            LaunchView()
        }
}

