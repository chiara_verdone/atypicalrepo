//
//  MapView.swift
//  AtypicalProject
//
//  Created by verdonechristian on 03/02/2020.
//  Copyright © 2020 verdonechiara. All rights reserved.
//

import SwiftUI
import MapKit

struct Map: View {
    var body: some View {
        MapView()
    }
}

struct MapView: UIViewRepresentable {
    
    func makeUIView(context: Context) -> MKMapView{
        return MKMapView()
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        
    }
}

struct Map_Previews: PreviewProvider {
    static var previews: some View {
        Map()
    }
}
